//==------ version.hpp --- SYCL compiler version macro ---------*- C++ -*---==//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#define __SYCL_COMPILER_VERSION 20250211
#define __LIBSYCL_MAJOR_VERSION 8
#define __LIBSYCL_MINOR_VERSION 0
#define __LIBSYCL_PATCH_VERSION 0
