//
// Copyright Contributors to the MaterialX Project
// SPDX-License-Identifier: Apache-2.0
//

#ifndef MATERIALX_GENERATED_H
#define MATERIALX_GENERATED_H

#define MATERIALX_MAJOR_VERSION 1
#define MATERIALX_MINOR_VERSION 39
#define MATERIALX_BUILD_VERSION 2

#define MATERIALX_BUILD_SHARED_LIBS

// Establish namespace:
namespace MaterialX_v1_39_2
{
}

// Establish alias to allow downstream clients to still use the MaterialX namespace:
namespace MaterialX = MaterialX_v1_39_2;

// All code in this project must use these macros for opening and closing the
// global MaterialX namespace:
#define MATERIALX_NAMESPACE_BEGIN namespace MaterialX_v1_39_2 {
#define MATERIALX_NAMESPACE_END }

#endif
